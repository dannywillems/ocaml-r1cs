(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2020-2021 Danny Willems <be.danny.willems@gmail.com>        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let rec repeat ?(n = 100) f =
  if n <= 0 then
    let f () = () in
    f
  else (
    f () ;
    repeat ~n:(n - 1) f )

module MakeVariable (R1CS : R1cs.T) = struct
  let test_one_variable () =
    assert (R1CS.(get_index one_var = 0)) ;
    assert (R1CS.(is_public one_var)) ;
    assert (not R1CS.(is_private one_var))

  let test_aux_variable_constructor () =
    let index = Random.int 1_000_000_000 in
    let r = R1CS.aux_var index in
    assert (R1CS.(get_index r = index)) ;
    assert (R1CS.(is_private r)) ;
    assert (not R1CS.(is_public r))

  let test_pub_variable_constructor () =
    let index = Random.int 1_000_000_000 in
    let r = R1CS.pub_var index in
    assert (R1CS.(get_index r = index)) ;
    assert (R1CS.(is_public r)) ;
    assert (not R1CS.(is_private r))

  let test_with_constraint_system_builder () =
    (* We check the indices are created one by one *)
    let r1cs = R1CS.empty in
    let (x, r1cs) = R1CS.new_pub_var r1cs in
    assert (R1CS.(get_index x = 0)) ;
    assert (R1CS.(is_public x)) ;
    assert (not R1CS.(is_private x)) ;
    let (x', _r1cs) = R1CS.new_aux_var r1cs in
    assert (R1CS.(get_index x' = 1)) ;
    assert (R1CS.(is_private x')) ;
    assert (not R1CS.(is_public x'))

  let get_tests () =
    let open Alcotest in
    ( Printf.sprintf "Variable helpers for field %s" (Z.to_string R1CS.Fp.order),
      [ test_case "Variable one" `Quick test_one_variable;
        test_case
          "Aux variable constructor"
          `Quick
          (repeat ~n:1000 test_aux_variable_constructor);
        test_case
          "With constraint system builder"
          `Quick
          test_with_constraint_system_builder;
        test_case
          "Public variable constructor"
          `Quick
          (repeat ~n:1000 test_pub_variable_constructor) ] )
end

module MakeConstructors (R1CS : R1cs.T) = struct
  let test_basic_constructors () = ignore R1CS.one

  let get_tests () =
    let open Alcotest in
    ( Printf.sprintf "Constructions for field %s" (Z.to_string R1CS.Fp.order),
      [test_case "basic" `Quick test_basic_constructors] )
end

module MakeValidity (R1CS : R1cs.T) = struct
  let generate_random_valid_linear_term num_variables =
    assert (num_variables >= 1) ;
    let rec aux acc i =
      if i = num_variables then acc
      else
        let r = R1CS.Fp.random () in
        let v =
          if Random.bool () then (R1CS.aux_var i, r) else (R1CS.pub_var i, r)
        in
        aux (v :: acc) (i + 1)
    in
    R1CS.linear_combination (aux [] 0)

  let test_basic_valid_term () =
    let r = R1CS.Fp.random () in
    let random_index = Random.int 100 in
    let random_linear_term = generate_random_valid_linear_term 100 in
    assert (R1CS.is_valid_term R1CS.one) ;
    assert (R1CS.is_valid_term (R1CS.linear_term (R1CS.pub_var random_index) r)) ;
    assert (R1CS.is_valid_term (R1CS.linear_term (R1CS.aux_var random_index) r)) ;
    assert (
      R1CS.(is_valid_term (linear_combination [(aux_var random_index, r)])) ) ;
    assert (
      R1CS.(is_valid_term (linear_combination [(pub_var random_index, r)])) ) ;
    assert (
      not
        R1CS.(
          is_valid_term
            (linear_combination
               [(pub_var random_index, r); (pub_var random_index, r)])) ) ;
    assert (
      not
        R1CS.(
          is_valid_term
            (R1CS.linear_combination
               [(aux_var random_index, r); (pub_var random_index, r)])) ) ;
    assert (
      not
        R1CS.(
          is_valid_term
            (R1CS.linear_combination
               [(pub_var random_index, r); (aux_var random_index, r)])) ) ;
    assert (
      not
        (R1CS.is_valid_term
           (R1CS.linear_combination
              [(R1CS.aux_var random_index, r); (R1CS.aux_var random_index, r)]))
    ) ;
    assert (R1CS.is_valid_term random_linear_term)

  let get_tests () =
    let open Alcotest in
    ( Printf.sprintf "Term validity for field %s" (Z.to_string R1CS.Fp.order),
      [test_case "basic" `Quick test_basic_valid_term] )
end

module MakeEvaluation (R1CS : R1cs.T) = struct
  let test_eval_term_test_vectors () =
    let test_vectors =
      [ ( R1CS.linear_term (R1CS.aux_var 3) R1CS.Fp.one,
          [R1CS.Fp.one; R1CS.Fp.zero; R1CS.Fp.zero; R1CS.Fp.zero],
          R1CS.Fp.zero );
        ( R1CS.linear_term (R1CS.aux_var 2) R1CS.Fp.one,
          [R1CS.Fp.one; R1CS.Fp.zero; R1CS.Fp.of_string "42"; R1CS.Fp.zero],
          R1CS.Fp.of_string "42" );
        ( R1CS.linear_combination
            [ (R1CS.aux_var 0, R1CS.Fp.one);
              (R1CS.pub_var 1, R1CS.Fp.of_string "42") ],
          [R1CS.Fp.one; R1CS.Fp.zero; R1CS.Fp.of_string "42"; R1CS.Fp.zero],
          R1CS.Fp.of_string "1" );
        ( R1CS.linear_combination
            [ (R1CS.aux_var 0, R1CS.Fp.one);
              (R1CS.pub_var 1, R1CS.Fp.of_string "42") ],
          [R1CS.Fp.one; R1CS.Fp.of_string "1"; R1CS.Fp.zero],
          R1CS.Fp.of_string "43" ) ]
    in
    List.iter
      (fun (term, eval_vector, expected_result) ->
        assert (R1CS.Fp.(R1CS.eval_term term eval_vector = expected_result)))
      test_vectors

  let test_r1cs_for_test_equations () =
    (* R1CS for x^3 + 1 = z ->
        - x * x = x'
        - x * x' = x''
        - 1 + x'' = z
       We need 5 variables -> x, x', x'', 1 and z and we have 3 constraints.
       We use index 1 for x, 2 for x', 3 for x'' and 4 for z. The only public variable is z.
    *)
    let r1cs = R1CS.empty in
    let (one, r1cs) = R1CS.new_pub_var ~name:"one" r1cs in
    let (x, r1cs) = R1CS.new_aux_var ~name:"x" r1cs in
    let (x', r1cs) = R1CS.new_aux_var ~name:"x'" r1cs in
    let (x'', r1cs) = R1CS.new_aux_var ~name:"x''" r1cs in
    let (z, r1cs) = R1CS.new_pub_var ~name:"z" r1cs in
    (* Let's now build the constraints *)
    let constraint_1 = R1CS.(of_terms (of_var x) (of_var x) (of_var x')) in
    let constraint_2 = R1CS.(of_terms (of_var x) (of_var x') (of_var x'')) in
    let constraint_3 =
      R1CS.of_terms
        (R1CS.linear_combination [(one, R1CS.Fp.one); (x'', R1CS.Fp.one)])
        R1CS.one
        (R1CS.of_var z)
    in
    let r1cs = R1CS.add_constraint r1cs constraint_1 in
    let r1cs = R1CS.add_constraint r1cs constraint_2 in
    let r1cs = R1CS.add_constraint r1cs constraint_3 in
    (* Let's take a non null element *)
    let r = R1CS.Fp.non_null_random () in
    assert (
      R1CS.is_satisfied
        r1cs
        [ R1CS.Fp.one;
          r;
          R1CS.Fp.mul r r;
          R1CS.Fp.(mul (mul r r) r);
          R1CS.Fp.(add (mul r (mul r r)) one) ] ) ;
    (* Checking a random assignment does not satisfy. This test might fail
       sometimes for small fields
    *)
    assert (
      not
        (R1CS.is_satisfied
           r1cs
           [ R1CS.Fp.non_null_random ();
             R1CS.Fp.non_null_random ();
             R1CS.Fp.non_null_random ();
             R1CS.Fp.non_null_random ();
             R1CS.Fp.non_null_random () ]) ) ;
    assert (R1CS.num_variables r1cs = 5) ;
    assert (R1CS.num_pub_variables r1cs = 2) ;
    assert (R1CS.num_aux_variables r1cs = 3)

  let get_tests () =
    let open Alcotest in
    ( Printf.sprintf "Term evaluation for field %s" (Z.to_string R1CS.Fp.order),
      [ test_case
          "Evaluation basic r1cs"
          `Quick
          (repeat ~n:10000 test_r1cs_for_test_equations);
        test_case "Basic evaluation of terms" `Quick test_eval_term_test_vectors
      ] )
end

module MakeEquality (R1CS : R1cs.T) = struct
  let test_basic_equality () =
    let r = R1CS.Fp.random () in
    (* On variables only *)
    assert (R1CS.eq R1CS.one R1CS.one) ;
    (* `one` constructor is always public and has index 0 *)
    assert (R1CS.eq (R1CS.linear_term (R1CS.pub_var 0) R1CS.Fp.one) R1CS.one) ;
    assert (
      R1CS.eq
        (R1CS.linear_term (R1CS.aux_var 1) r)
        (R1CS.linear_term (R1CS.aux_var 1) r) ) ;
    assert (
      R1CS.eq
        (R1CS.linear_term (R1CS.pub_var 1) r)
        (R1CS.linear_term (R1CS.pub_var 1) r) ) ;
    assert (
      not
        (R1CS.eq
           (R1CS.linear_term (R1CS.pub_var 1) r)
           (R1CS.linear_term (R1CS.aux_var 1) r)) ) ;
    assert (
      not
        (R1CS.eq
           (R1CS.linear_term (R1CS.aux_var 1) r)
           (R1CS.linear_term (R1CS.pub_var 1) r)) ) ;
    assert (
      not
        (R1CS.eq
           (R1CS.linear_term (R1CS.pub_var 2) r)
           (R1CS.linear_term (R1CS.pub_var 1) r)) ) ;
    assert (
      not
        (R1CS.eq
           (R1CS.linear_term (R1CS.pub_var 1) r)
           (R1CS.linear_term (R1CS.pub_var 2) r)) ) ;
    assert (
      not
        (R1CS.eq
           (R1CS.linear_term (R1CS.aux_var 2) r)
           (R1CS.linear_term (R1CS.aux_var 1) r)) ) ;
    assert (
      not
        (R1CS.eq
           (R1CS.linear_term (R1CS.aux_var 1) r)
           (R1CS.linear_term (R1CS.aux_var 2) r)) ) ;
    assert (
      not
        (R1CS.eq
           (R1CS.linear_term (R1CS.aux_var 1) r)
           (R1CS.linear_term (R1CS.aux_var 2) r)) )

  let get_tests () =
    let open Alcotest in
    ( Printf.sprintf "Equality for field %s" (Z.to_string R1CS.Fp.order),
      [test_case "basic" `Quick test_basic_equality] )
end

module MakeMatrices (R1CS : R1cs.T) = struct
  let r1cs =
    (* Example 1, simple 1 * x = x. One constraint, x is private *)
    let r1cs = R1CS.empty in
    let (one, r1cs) = R1CS.new_pub_var ~name:"one" r1cs in
    let (x, r1cs) = R1CS.new_aux_var r1cs in
    R1CS.add_constraint
      r1cs
      (R1CS.of_terms (R1CS.of_var one) (R1CS.of_var x) (R1CS.of_var x))

  let test_get_dense_matrices () =
    let expected_result_dense =
      R1CS.
        ( [| [| Fp.one; Fp.zero |] |],
          [| [| Fp.zero; Fp.one |] |],
          [| [| Fp.zero; Fp.one |] |] )
    in
    assert (expected_result_dense = R1CS.get_dense_matrices r1cs)

  let test_get_sparse_matrices () =
    let expected_result_sparse =
      R1CS.([[(0, Fp.one)]], [[(1, Fp.one)]], [[(1, Fp.one)]])
    in
    assert (expected_result_sparse = R1CS.get_sparse_matrices r1cs)

  let get_tests () =
    let open Alcotest in
    ( Printf.sprintf "Matrices for field %s" (Z.to_string R1CS.Fp.order),
      [ test_case
          "Get dense matrices on test vectors"
          `Quick
          test_get_dense_matrices;
        test_case
          "Get sparse matrices on test vectors"
          `Quick
          test_get_sparse_matrices ] )
end

let make_test_batteries_for_p p =
  let module Fp = Ff.MakeFp (struct
    let prime_order = p
  end) in
  let module R1CS = R1cs.MakeR1CS (Fp) in
  let module EqualityFp = MakeEquality (R1CS) in
  let module ValidityFp = MakeValidity (R1CS) in
  let module ConstructorsFp = MakeConstructors (R1CS) in
  let module EvaluationFp = MakeEvaluation (R1CS) in
  let module VariablesFp = MakeVariable (R1CS) in
  let module MatricesFp = MakeMatrices (R1CS) in
  [ ConstructorsFp.get_tests ();
    ValidityFp.get_tests ();
    EvaluationFp.get_tests ();
    MatricesFp.get_tests ();
    VariablesFp.get_tests ();
    EqualityFp.get_tests () ]

let () =
  let open Alcotest in
  run
    "R1CS"
    (List.concat
       [ make_test_batteries_for_p
           (Z.of_string
              "52435875175126190479447740508185965837690552500527637822603658699938581184513");
         make_test_batteries_for_p (Z.of_string "379");
         make_test_batteries_for_p Z.(pow (succ one) 255 - of_int 19);
         make_test_batteries_for_p
           (Z.of_string
              "4002409555221667393417789825735904156556882819939007885332058136124031650490837864442687629129015664037894272559787")
       ])
