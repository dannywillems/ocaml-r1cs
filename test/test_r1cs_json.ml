(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2021 Danny Willems <be.danny.willems@gmail.com>             *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* FIXME: add test checking
- failure when the field order is not the sin
- version is not supported
- when the file is not correctly formatted
*)

module Fp = Ff.MakeFp (struct
  let prime_order =
    Z.of_string
      "52435875175126190479447740508185965837690552500527637822603658699938581184513"
end)

module R1CS = R1cs.MakeR1CS (Fp)

let test_load_and_eval filename () =
  let (circuit, evaluations) =
    R1cs_json.from_file (module Fp) (module R1CS) filename
  in
  assert (R1CS.is_satisfied circuit evaluations)

let test_load_and_generate_matrices filename () =
  let (circuit, _evaluations) =
    R1cs_json.from_file (module Fp) (module R1CS) filename
  in
  ignore @@ R1CS.get_dense_matrices circuit

let () =
  let open Alcotest in
  run
    "R1CS JSON"
    [ ( "Load and generate matrices",
        [ (* Pedersen hash *)
          test_case
            "Pedersen hash with evaluation vector"
            `Quick
            (test_load_and_generate_matrices
               "pedersen-hash-with-evaluation.r1cs");
          test_case
            "Pedersen hash without evaluation vector"
            `Quick
            (test_load_and_generate_matrices "pedersen-hash.r1cs");
          (* Merkle tree - Commented because OOM *)
          (* test_case
           *   "Merkle tree path with evaluation vector"
           *   `Quick
           *   (test_load_and_generate_matrices "merkle-path-with-evaluation.r1cs");
           * test_case
           *   "Merkle tree path without evaluation vector"
           *   `Quick
           *   (test_load_and_generate_matrices "merkle-path.r1cs"); *)
          test_case
            "Simple with evaluation vector"
            `Quick
            (test_load_and_generate_matrices "simple-with-evaluation.r1cs");
          test_case
            "Simple without evaluation vector"
            `Quick
            (test_load_and_generate_matrices "simple.r1cs") ] );
      ( "Load and evaluate",
        [ (* Pedersen hash *)
          test_case
            "Pedersen hash with evaluation vector"
            `Quick
            (test_load_and_eval "pedersen-hash-with-evaluation.r1cs");
          (* Merkle tree *)
          test_case
            "Merkle tree path with evaluation vector"
            `Quick
            (test_load_and_eval "merkle-path-with-evaluation.r1cs");
          test_case
            "Simple with evaluation vector"
            `Quick
            (test_load_and_eval "simple-with-evaluation.r1cs") ] ) ]
