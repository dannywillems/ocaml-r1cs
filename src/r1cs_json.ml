(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2021 Danny Willems <be.danny.willems@gmail.com>             *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* FIXME:
- raise specific exceptions for field characteristique being different, extension degree being different than one, etc
- add verbose mode (like when names are ignored)
*)
let supported_versions = ["1.0"]

type header =
  { version : string;
    field_characteristic : string;
    extension_degree : string;
    instances : int;
    witnesses : int;
    constraints : int;
    optimized : bool
  }
[@@deriving yojson { exn = true }]

type t =
  { r1cs : header;
    names : (int * string) list;
    a : (int * string) list list;
    b : (int * string) list list;
    c : (int * string) list list;
    evaluations : (int * string) list
  }
[@@deriving yojson { exn = true }]

let from_file (type a b) (module Fp : Ff_sig.PRIME with type t = b)
    (module R1CS : R1cs.T with type t = a and type Fp.t = b) filename =
  let t = Yojson.Safe.from_file filename |> of_yojson_exn in
  assert (Z.equal Fp.order (Z.of_string t.r1cs.field_characteristic)) ;
  assert (List.mem t.r1cs.version supported_versions) ;
  (* Only ext degree 1 is supported, this is a general fact with R1CS *)
  assert (t.r1cs.extension_degree = "1") ;
  (* We slide the indices below. Consistency with the variable one being always
     present and with the index 0 *)
  assert (t.r1cs.instances >= 1) ;
  let names =
    List.fast_sort
      (fun (x, _) (y, _) -> x - y)
      (List.map
         (fun (index, name) ->
           if index <= 0 then (-index, name)
           else (t.r1cs.instances + index - 1, name))
         t.names)
  in
  (* Keep evaluations *)
  let evaluations =
    List.map
      snd
      (List.fast_sort
         (fun (x, _) (y, _) -> x - y)
         (List.map
            (fun (index, value) ->
              let value = Fp.of_string value in
              if index <= 0 then (-index, value)
              else (t.r1cs.instances + index - 1, value))
            t.evaluations))
  in
  (* Building the r1cs, starting with allocating the variables *)
  let r1cs =
    List.fold_left
      (fun r1cs (idx, name) ->
        let (v, r1cs) =
          if idx < t.r1cs.instances then R1CS.new_pub_var r1cs ~name
          else R1CS.new_aux_var r1cs ~name
        in
        (* Additional check verifying the index are correctly assigned *)
        assert (R1CS.get_index v = idx) ;
        r1cs)
      R1CS.empty
      names
  in
  assert (R1CS.num_aux_variables r1cs = t.r1cs.witnesses) ;
  assert (R1CS.num_pub_variables r1cs = t.r1cs.instances) ;
  let variables =
    List.fast_sort
      (fun v1 v2 -> R1CS.get_index v1 - R1CS.get_index v2)
      (R1CS.get_variables r1cs)
  in
  let convert_constraint c =
    List.map
      (fun (idx, s) ->
        let slided_idx =
          if idx <= 0 then -idx else t.r1cs.instances + idx - 1
        in
        let v = List.nth variables slided_idx in
        (v, Fp.of_string s))
      c
  in
  let add_constraint r1cs a b c =
    let a = convert_constraint a in
    let b = convert_constraint b in
    let c = convert_constraint c in
    R1CS.add_constraint
      r1cs
      (R1CS.of_terms
         (R1CS.linear_combination a)
         (R1CS.linear_combination b)
         (R1CS.linear_combination c))
  in
  let constraints = List.combine (List.combine t.a t.b) t.c in
  let r1cs =
    List.fold_left
      (fun r1cs ((a, b), c) -> add_constraint r1cs a b c)
      r1cs
      constraints
  in
  assert (R1CS.num_constraints r1cs = t.r1cs.constraints) ;
  (r1cs, evaluations)
