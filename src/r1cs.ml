(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2020-2021 Danny Willems <be.danny.willems@gmail.com>        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Main entrypoint for the library

    Building a constraint system over a finite field [Fp] is done using the functor [MakeR1CS].
    You can use the library [Ff] to generate a finite field compatible with the interface.
    Allocating variables in the system is done using the functions [new_aux_var] and [new_pub_var].

    Have a look at the README.md for a full working example.

    IMPROVEME:
    - Add J-R1CS exporter
*)

type 'a matrix = 'a Array.t Array.t

type 'a sparse_matrix = (int * 'a) list list

module type T = sig
  module Fp : Ff_sig.PRIME

  (** Represent a variable, private or public *)
  type var

  (** [pub_var name index] creates a public variable at the index [index] and
      with the name [name].

      NB: when building a constraint system, use [new_pub_var] to allocate new
      variables and use the resulting variable in the constraint
  *)
  val pub_var : ?name:string -> int -> var

  (** [aux_var name index] creates a public variable at the index [index] and
      with the name [name].

      NB: when building a constraint system, use [new_aux_var] to allocate new
      variables and use the resulting variable in the constraint
  *)
  val aux_var : ?name:string -> int -> var

  (** Returns the variable for the constant [1] *)
  val one_var : var

  val get_index : var -> int

  val get_label : var -> string option

  val is_private : var -> bool

  val is_public : var -> bool

  (* Terms *)
  type _ term

  (** Verify the given term is a valid term *)
  val is_valid_term : Fp.t term -> bool

  (** [eval_term term assignment] evaluates the term with the given assignment
      [assignment] *)
  val eval_term : Fp.t term -> Fp.t list -> Fp.t

  (** Returns the term representing the constant [1] *)
  val one : Fp.t term

  (** Build a term from a variable *)
  val of_var : var -> Fp.t term

  (** [linear_term x a] build a linear term reprenting a * x *)
  val linear_term : var -> Fp.t -> Fp.t term

  (** Build a linear combinaison of terms *)
  val linear_combination : (var * Fp.t) list -> Fp.t term

  val get_variables_of_term : Fp.t term -> var list

  (** [eq t1 t2] returns [true] if [t1] and [t2] are the same terms. Variable
      names are ignored as there are annotations.
  *)
  val eq : Fp.t term -> Fp.t term -> bool

  (** One constraint, [A * B = C] *)
  type rank_one_constraint

  (** Build a constraint from the terms [A], [B] and [C] *)
  val of_terms : Fp.t term -> Fp.t term -> Fp.t term -> rank_one_constraint

  (** Get the term [A] from the given constraint *)
  val get_a : rank_one_constraint -> Fp.t term

  (** Get the term [B] from the given constraint *)
  val get_b : rank_one_constraint -> Fp.t term

  (** Get the term [C] from the given constraint *)
  val get_c : rank_one_constraint -> Fp.t term

  (** Constraint system, a list of constraints *)
  type t

  exception Invalid_assignment_length of t * int * Fp.t list

  (** Create an empty constraint system *)
  val empty : t

  (** Allocate a new auxiliary variable in the constraint system with the name
      [name] and returns the new variable with the modified r1cs *)
  val new_aux_var : ?name:string -> t -> var * t

  (** Allocate a new public variable in the constraint system with the name
      [name] and returns the new variable with the modified r1cs *)
  val new_pub_var : ?name:string -> t -> var * t

  (** Add a constraint to the sytem *)
  val add_constraint : t -> rank_one_constraint -> t

  (** Returns the variables of the constraint system as allocated in the
      system (including the names). *)
  val get_variables : t -> var list

  (** Returns the total number of variables in the constraint system *)
  val num_variables : t -> int

  (** Returns the number of public variables in the constraint system *)
  val num_pub_variables : t -> int

  (** Returns the number of auxiliary variables in the constraint system *)
  val num_aux_variables : t -> int

  val num_constraints : t -> int

  (** Returns the constraints of the system *)
  val constraints : t -> rank_one_constraint list

  (** [is_satisfied r1cs assignment] verifies the constraint system is satisfied
      when giving the assignment [assignment] *)
  val is_satisfied : t -> Fp.t list -> bool

  (** Returns the constraint system as dense matrices *)
  val get_dense_matrices : t -> Fp.t matrix * Fp.t matrix * Fp.t matrix

  (** Similar to [get_dense_matrices] but constraints are returned as lists of
      indices and values containing only non-null values *)
  val get_sparse_matrices :
    t -> Fp.t sparse_matrix * Fp.t sparse_matrix * Fp.t sparse_matrix
end

module MakeR1CS (F : Ff_sig.PRIME) : T with type Fp.t = F.t = struct
  module Fp = F

  type var = Aux of int * string option | Pub of int * string option

  let aux_var ?name i = Aux (i, name)

  let pub_var ?name i = Pub (i, name)

  let one_var = Pub (0, Some "One")

  let get_index v = match v with Aux (i, _) | Pub (i, _) -> i

  let is_public v = match v with Pub _ -> true | _ -> false

  let is_private v = match v with Aux _ -> true | _ -> false

  let get_label v = match v with Aux (_, s) | Pub (_, s) -> s

  type _ term =
    | Var : var -> Fp.t term
    | Lt : (var * Fp.t) -> Fp.t term
    (* Invariant: No incompatible variable and duplicated variables (incompatible
       means for a same index, public and aux
       TODO: encode this invariant in the type
    *)
    | Lc : (var * Fp.t) list -> Fp.t term

  let is_valid_term t =
    match t with
    | Var _ -> true
    | Lt _ -> true
    | Lc lts ->
        (* check no duplicated indices *)
        let indices =
          List.map
            (fun (var, _s) -> match var with Aux (i, _s) | Pub (i, _s) -> i)
            lts
        in
        let ordered_indices = List.sort_uniq Int.compare indices in
        List.length ordered_indices = List.length indices

  let rec eval_term term eval_vector =
    match term with
    | Var (Pub (i, _s)) | Var (Aux (i, _s)) ->
        if i < List.length eval_vector then List.nth eval_vector i
        else failwith "Error"
    | Lt (Aux (i, _s), s) | Lt (Pub (i, _s), s) ->
        if i < List.length eval_vector then Fp.(List.nth eval_vector i * s)
        else failwith "Error"
    | Lc lcs ->
        List.fold_left
          (fun acc (var, s) -> Fp.(acc + eval_term (Lt (var, s)) eval_vector))
          Fp.zero
          lcs

  let one = Var (Pub (0, Some "Constant one"))

  let of_var v = Var v

  let linear_term var value = Lt (var, value)

  let linear_combination lts = Lc lts

  let get_variables_of_term term =
    match term with
    | Var v -> [v]
    | Lt (v, _) -> [v]
    | Lc lts -> List.map fst lts

  let rec eq term1 term2 =
    match (term1, term2) with
    | (Var (Pub (i, _s)), Var (Pub (j, _))) -> i = j
    | (Var (Aux (i, _s)), Var (Aux (j, _))) -> i = j
    | (Var _, Lt (x1, a1)) when Fp.(is_one a1) -> eq term1 (Var x1)
    | (Lt (x1, a1), Var _) when Fp.(is_one a1) -> eq (Var x1) term2
    | (Lt (x1, a1), Lt (x2, a2)) -> eq (Var x1) (Var x2) && Fp.(a1 = a2)
    | (Lt (x1, a1), Lc [(x2, a2)]) -> eq (Var x1) (Var x2) && Fp.(a1 = a2)
    | (Lc lcs, Lc lcs') ->
        if List.length lcs <> List.length lcs' then false
        else
          List.for_all
            (fun (lt, lt') -> eq (Lt lt) (Lt lt'))
            (List.combine lcs lcs')
    | _ -> false

  type rank_one_constraint = { a : F.t term; b : F.t term; c : F.t term }

  let of_terms a b c = { a; b; c }

  let get_a r1c = r1c.a

  let get_b r1c = r1c.b

  let get_c r1c = r1c.c

  type t =
    { constraints : rank_one_constraint list;
      num_pub_variables : int;
      num_aux_variables : int;
      variables : var list
    }

  let new_aux_var ?name r1cs =
    let current_index = r1cs.num_aux_variables + r1cs.num_pub_variables in
    let var = Aux (current_index, name) in
    let r1cs =
      { constraints = r1cs.constraints;
        num_pub_variables = r1cs.num_pub_variables;
        num_aux_variables = r1cs.num_aux_variables + 1;
        variables = var :: r1cs.variables
      }
    in
    (var, r1cs)

  let new_pub_var ?name r1cs =
    let current_index = r1cs.num_pub_variables + r1cs.num_aux_variables in
    let var = Pub (current_index, name) in
    let r1cs =
      { constraints = r1cs.constraints;
        num_pub_variables = r1cs.num_pub_variables + 1;
        num_aux_variables = r1cs.num_aux_variables;
        variables = var :: r1cs.variables
      }
    in
    (var, r1cs)

  exception Invalid_assignment_length of t * int * Fp.t list

  let empty =
    { constraints = [];
      num_pub_variables = 0;
      num_aux_variables = 0;
      variables = []
    }

  let add_constraint r1cs r1c =
    { constraints = r1c :: r1cs.constraints;
      num_pub_variables = r1cs.num_pub_variables;
      num_aux_variables = r1cs.num_aux_variables;
      variables = r1cs.variables
    }

  let num_variables t = t.num_pub_variables + t.num_aux_variables

  let num_pub_variables t = t.num_pub_variables

  let num_aux_variables t = t.num_aux_variables

  let num_constraints t = List.length t.constraints

  let get_variables t = t.variables

  let constraints t = t.constraints

  let get_number_of_constraints t = List.length t.constraints

  let is_satisfied r1cs eval_vector =
    let length_assignment = List.length eval_vector in
    if length_assignment <> num_variables r1cs then
      raise (Invalid_assignment_length (r1cs, length_assignment, eval_vector))
    else
      List.for_all
        (fun r1c ->
          Fp.(
            eq
              (eval_term r1c.a eval_vector * eval_term r1c.b eval_vector)
              (eval_term r1c.c eval_vector)))
        r1cs.constraints

  let get_dense_matrices r1cs : Fp.t matrix * Fp.t matrix * Fp.t matrix =
    let matrix_a =
      Array.make_matrix
        (get_number_of_constraints r1cs)
        (num_variables r1cs)
        Fp.zero
    in
    let matrix_b =
      Array.make_matrix
        (get_number_of_constraints r1cs)
        (num_variables r1cs)
        Fp.zero
    in
    let matrix_c =
      Array.make_matrix
        (get_number_of_constraints r1cs)
        (num_variables r1cs)
        Fp.zero
    in
    let get_dense_term term num_variables : Fp.t Array.t =
      let res = Array.make num_variables Fp.zero in
      ( match term with
      (* duplicating for future changes *)
      | Var (Pub (index, _)) -> res.(index) <- Fp.one
      | Var (Aux (index, _)) -> res.(index) <- Fp.one
      | Lt (Pub (index, _), s) -> res.(index) <- s
      | Lt (Aux (index, _), s) -> res.(index) <- s
      | Lc lts ->
          List.iter
            (fun (var, s) ->
              match var with
              | Pub (index, _) -> res.(index) <- s
              | Aux (index, _) -> res.(index) <- s)
            lts ) ;
      res
    in
    List.iteri
      (fun i { a; b; c } ->
        matrix_a.(i) <- get_dense_term a (num_variables r1cs) ;
        matrix_b.(i) <- get_dense_term b (num_variables r1cs) ;
        matrix_c.(i) <- get_dense_term c (num_variables r1cs))
      r1cs.constraints ;
    (matrix_a, matrix_b, matrix_c)

  let get_sparse_matrices r1cs :
      (int * Fp.t) list list * (int * Fp.t) list list * (int * Fp.t) list list =
    let list_a = ref [] in
    let list_b = ref [] in
    let list_c = ref [] in

    let get_sparse_term term : (int * Fp.t) list =
      let res = ref [] in
      ( match term with
      | Var (Pub (index, _)) -> res := (index, Fp.one) :: !res
      | Var (Aux (index, _)) -> res := (index, Fp.one) :: !res
      | Lt (Pub (index, _), s) -> res := (index, s) :: !res
      | Lt (Aux (index, _), s) -> res := (index, s) :: !res
      | Lc lts ->
          List.iter
            (fun (var, s) ->
              match var with
              | Pub (index, _) -> res := (index, s) :: !res
              | Aux (index, _) -> res := (index, s) :: !res)
            lts ) ;
      List.rev !res
    in

    List.iter
      (fun { a; b; c } ->
        list_a := get_sparse_term a :: !list_a ;
        list_b := get_sparse_term b :: !list_b ;
        list_c := get_sparse_term c :: !list_c)
      r1cs.constraints ;
    (List.rev !list_a, List.rev !list_b, List.rev !list_c)
end
