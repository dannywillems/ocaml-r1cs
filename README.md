# ocaml-r1cs

Play with Rank One Constraint System.

Building a constraint system over a (prime) finite field `Fp` is done using the
functor `MakeR1CS`.
You can use the library [Ff](https://gitlab.com/dannywillems/ocaml-ff) to
generate a finite field compatible with the interface.
Allocating variables in the system is done using the functions `new_aux_var` and
`new_pub_var`.

Here a full example on how to use the library

```ocaml
module Fp = Ff.MakeFp(
struct
  let prime_order =
    (Z.of_string
      "52435875175126190479447740508185965837690552500527637822603658699938581184513")
end)
module R1CS = R1cs.MakeR1CS(Fp)

(* R1CS for x^3 + 1 = z ->
    - x * x = x'
    - x * x' = x''
    - 1 + x'' = z
   We need 5 variables -> x, x', x'', 1 and z and we have 3 constraints.
   We use index 1 for x, 2 for x', 3 for x'' and 4 for z. The only public variable is z.
*)
let r1cs = R1CS.empty in
let (x, r1cs) = R1CS.new_aux_var ~name:"x" r1cs in
let (x', r1cs) = R1CS.new_aux_var ~name:"x'" r1cs in
let (x'', r1cs) = R1CS.new_aux_var ~name:"x''" r1cs in
let (z, r1cs) = R1CS.new_pub_var ~name:"z" r1cs in
(* Let's now build the constraints *)
let constraint_1 = R1CS.(of_terms (of_var x) (of_var x) (of_var x')) in
let constraint_2 = R1CS.(of_terms (of_var x) (of_var x') (of_var x'')) in
let constraint_3 =
  R1CS.of_terms
    (R1CS.linear_combination
       [(R1CS.one_var, R1CS.Fp.one); (x'', R1CS.Fp.one)])
    R1CS.one
    (R1CS.of_var z)
in
let r1cs = R1CS.add_constraint r1cs constraint_1 in
let r1cs = R1CS.add_constraint r1cs constraint_2 in
let r1cs = R1CS.add_constraint r1cs constraint_3 in
(* Let's take a non null element *)
let r = R1CS.Fp.non_null_random () in
assert (
  R1CS.is_satisfied
    r1cs
    [ R1CS.Fp.one;
      r;
      R1CS.Fp.mul r r;
      R1CS.Fp.(mul (mul r r) r);
      R1CS.Fp.(add (mul r (mul r r)) one) ] ) ;
```

It is possible to get the constraint system as dense matrices using the function `get_dense_matrices`.


## Local environment

```
opam switch create . 4.11.0
opam install -y . --with-test
```

- Run tests
```
dune build @runtest
```

- Get test coverage
```
dune runtest --instrument-with bisect_ppx --force
bisect-ppx-report html
```
