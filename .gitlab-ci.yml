stages:
  - lint
  - build
  - doc
  - coverage
  - deploy

lint:
  stage: lint
  image: registry.gitlab.com/dannywillems/docker-ocamlformat:0.15.0
  before_script:
    - eval $(opam env)
  script:
    - ocamlformat --check src/*.ml*
    - ocamlformat --check test/*.ml*

.build-test: &build_definition
  stage: build
  before_script:
    # Always update system package manager + setup OPAM env
    - sudo apt-get update && sudo apt-get upgrade -y
    - eval $(opam env)
    - opam repository set-url default https://opam.ocaml.org
    - opam update
    - opam depext -i -y zarith
  script:
    - opam install --deps-only --with-test -y .
    - opam pin add r1cs.dev ./ --no-action
    - opam pin add r1cs-json.dev ./ --no-action
    # Install the package
    - opam install r1cs
    - opam install r1cs-json

build-ocaml-4.08:
  <<: *build_definition
  image: ocaml/opam2:4.08

build-ocaml-4.09:
  <<: *build_definition
  image: ocaml/opam2:4.09

build-ocaml-4.10:
  <<: *build_definition
  image: ocaml/opam2:4.10

build-ocaml-4.11:
  <<: *build_definition
  image: ocaml/opam2:4.11

doc:
  stage: doc
  image: ocaml/opam2:4.09
  before_script:
    - sudo apt-get update && sudo apt-get upgrade -y
    - eval $(opam env)
    - opam repository set-url default https://opam.ocaml.org
    - opam update
    - opam depext -i -y zarith
    - sudo apt-get install m4 -y
    - opam install odoc -y
    - opam pin add r1cs.dev ./ --no-action
    - opam pin add r1cs-json.dev ./ --no-action
    # Install the package
    - opam install r1cs
    - opam install r1cs-json
  script:
    - dune build @doc
    - cp -r _build/default/_doc/_html odoc/
  artifacts:
    paths:
      - odoc

# Coverage and publish to coveralls
coveralls:
  stage: coverage
  image: ocaml/opam2:4.10
  before_script:
    - sudo apt-get install m4 jq -y
    - eval $(opam env)
    - opam repository set-url default https://opam.ocaml.org
    - opam update
    - opam depext -i -y zarith
    - opam install . -y --with-test
  script:
    - dune runtest --instrument-with bisect_ppx --force
    - bisect-ppx-report coveralls coverage-raw.json --coverage-path _build/default/
    - ./_ci/coverage_metadata_glci.sh coverage-raw.json > coverage_glci.json
    - curl --location --request POST 'https://coveralls.io/api/v1/jobs' --form 'json_file=@coverage_glci.json'

# Build the documentation, but do not publish
pages:
  stage: deploy
  image: ocaml/opam2:4.09
  before_script:
    - sudo apt-get update && sudo apt-get upgrade -y
    - eval $(opam env)
    - opam repository set-url default https://opam.ocaml.org
    - opam update
    - opam depext -i -y zarith
    - sudo apt-get install m4 -y
    - opam install odoc -y
    - opam pin add r1cs.dev ./ --no-action
    - opam pin add r1cs-json.dev ./ --no-action
    # Install the package
    - opam install r1cs
    - opam install r1cs-json
  script:
    - dune build @doc
    - cp -r _build/default/_doc/_html public/
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
